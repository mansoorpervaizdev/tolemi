import datetime


def timestamp():
    '''
    This function returns the current time for using in db models
    Returns
    -------
    datetime: current time
    '''
    return datetime.datetime.utcnow()
