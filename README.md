# Records Keeper
This restful api will low property dataset that can be downloaded from [​here](https://data.providenceri.gov/Finance/2017-Property-Tax-Roll/ku9m-5rhr​).
And serve the data with an endpoint [http://127.0.0.1:5000/record/zero-based-index

## Installation Instructions
### Setup Environment
create a virtual env for python 3
`virtualenv -p python3 venv`

and activate it
`source venv/bin/activate`

Install requirements
`pip install -r requirements.txt`

### Setup Application
The application uses SQLlite so create the db using the following command
`python manage.py create_db`

Then load data file after downloading:
`python manage.py load_data -f /path/to/file

Then run the application
`python manage.py runserver`


## Notes for evaluator
- You will find the endpoint in app/api/record_keeper.py and crux of the business logic and db model in app/models.py

### Shortcomings and possible improvements

- Currently there are no tests for this code which need to be added

- I believe that if all the data files are like this a document like db might be a better way to go

- If we stick with sql for large data requirement I would use something like Vitress which will help with sharding and allow us to manage the large data set

- For data load (in case of a large file size) the best solution might be to split the file in manageable chunks and use asynchio to improve performance.

- I believe due to the which level of IO asynch IO will be helpful even without spliting the incoming large file

- For a dataszie of larger than 1TB, I would split the input multi tb file (I am assuming that there are tools that will allow us to do this quite efficiently).
Then I would have sets of microservices. 1. row readers that will read one of these files and push the row into a rabbitmq or kafka queue and another set of microservices 'db_writers' that will just
read the entries from the messaging queue and write to db.
