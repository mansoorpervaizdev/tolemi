from flask import Flask
from flask_sqlalchemy import SQLAlchemy


def create_app():
    SQL_DB_URI = 'sqlite:///../records.db'
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = SQL_DB_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    from app.api import api as api_blueprint
    app.register_blueprint(api_blueprint)
    return app


db = SQLAlchemy()
app = create_app()
