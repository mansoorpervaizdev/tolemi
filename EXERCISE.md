# Exercise
Write a small web-server in a language of your choice that provides a
single API endpoint that receives an index (0 based) and returns the
corresponding record from the dataset found [​here](https://data.providenceri.gov/Finance/2017-Property-Tax-Roll/ku9m-5rhr​).


For example:
`/record/​<index>` Would return:
```
{
    "class": "80",
    "class_desc": "State",
    "formatted_address": "Bridge Across Seekonk River", "levy_code": "E01",
    ...
}
```


## Note:
* How you store and retrieve the data is your choice
* The solution does not need to be optimized for performance

# VERY IMPORTANT
Please provide us with working code and instructions on how to run. Document your code and explain your thought process so we can understand how you work. Also explain what are the trade-offs of your solution and how you would fix them in a production environment.


How would you change your solution if the dataset had 50M records or was bigger than 1TB?