from sqlalchemy import Column, DateTime, Integer, Float, String, UniqueConstraint
from sqlalchemy.sql import sqltypes
from sqlalchemy.exc import IntegrityError, OperationalError
from app import db
from app.utils import timestamp
import csv
from app.exceptions import ColumnError

Base = db.Model


class Property(Base):
    '''
    Table that will maintain all the data for the properties.
    This table has a unique constraint on Plat Lot Unit
    '''
    __table_args__ = (
        UniqueConstraint('plat_lot_unit', name='unique_plat_lot_unit'),
    )
    id = Column(Integer, primary_key=True)
    plat_lot_unit = Column(String)
    plat = Column(Integer)
    lot = Column(Integer)
    unit = Column(String)
    p_class = Column(Integer)
    class_desc = Column(String)
    levy_code = Column(String)
    levy_code_desc = Column(String)
    location_number = Column(String)
    location_street = Column(String)
    location_suffix = Column(String)
    formatted_address = Column(String)
    location_city = Column(String)
    location_zip = Column(String)
    owner_first_name = Column(String)
    owner_last_name = Column(String)
    owner_company = Column(String)
    owner_number = Column(String)
    owner_street = Column(String)
    owner_suffix = Column(String)
    owner_unit = Column(String)
    owner_city = Column(String)
    owner_state = Column(String)
    owner_zip = Column(String)
    total_assessment = Column(Float)
    total_exemption = Column(Float)
    total_taxes = Column(Float)
    property_location = Column(String)
    owner_location = Column(String)
    __datetime_created = Column(DateTime, nullable=False, default=timestamp)

    def get_dict(self):
        '''
        Returns the values of the attributes in the table for a row in a dictionary format
        :return:
        dict: populated with data of one row
        '''
        returnable_members = [attr for attr in dir(self)
                              if not callable(getattr(self, attr))
                              and not attr.startswith("_")
                              and not attr == 'query'
                              and not attr == 'metadata'
                              ]

        return_dict = {}
        for returnable_member in returnable_members:
            dict_key = returnable_member[2:] if returnable_member == 'p_class' else returnable_member
            return_dict[dict_key] = getattr(self, returnable_member)
        return return_dict

    @staticmethod
    def get_object_from_csv_row(csv_row):
        '''
        Creates a Property object from a csv row

        :param csv_row: is a row in a csv file (data file)
        :return:
        Property object representing the csv row
        '''
        property = Property()
        try:
            for column_name in csv_row:
                column_value = csv_row[column_name]

                column_name = (column_name.replace(' ', '_')
                               ).replace('-', '_').replace('.', '').lower()
                column_name = 'p_'+column_name if column_name == 'class' else column_name

                column_type = find_type(Property, column_name)

                if type(column_type) == sqltypes.Float:
                    column_value = 0.0 if column_value is '' else float(column_value)
                elif type(column_type) == sqltypes.Integer:
                    column_value = 0 if column_value is '' else int(column_value)

                setattr(property, column_name, column_value)
        except NameError as ne:
            raise ColumnError(f'{ne} is not a supported column')
        return property


def find_type(class_, colname):
    '''
    Returns the type of sqlalchemy column passed to it for a table / class

    :param class_: Is the sqlalchemy class associated with the table entry in the db
    :param colname:  is the name of the column for which a type is required..
    :return:
    SqlAlchemy column type
    '''
    if hasattr(class_, '__table__') and colname in class_.__table__.c:
        return class_.__table__.c[colname].type
    for base in class_.__bases__:
        return find_type(base, colname)
    raise NameError(colname)


def load_csv_file(file_path):
    '''
    Takes a path to a csv file and for every row makes and entry in the
    db for a Property object

    :param file_path: str path to the data input file
    :return:
    void
    '''
    csv_reader = None

    try:
        file = open(file_path, 'r')
        csv_reader = csv.DictReader(file, delimiter=',')
    except IOError:
        print(f'Unable to open data file {file_path}')
        exit(0)
    except TypeError:
        print('No file passed')
        exit(0)
    line_counter = 0
    for row in csv_reader:
        try:
            line_counter += 1
            property = Property.get_object_from_csv_row(row)
            db.session.add(property)
            db.session.commit()
        except IOError:
            print(f'Unable to read file {file_path} at line# {line_counter}')
        except IntegrityError:
            print(f'Property with plat lot unit: {property.plat_lot_unit} already exists')
            db.session.rollback()
        except OperationalError:
            print('Error: db not set up. Did you run "python manage.py create_db"?')
            exit(0)
        except ColumnError:
            print(ColumnError)
            exit(0)
