from flask import Blueprint

api = Blueprint('api', __name__)

from . import record_keeper  # noqa F401 E402 ignore because we need to import it here after Blueprint has been set up
