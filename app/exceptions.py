class NoDBSetup(Exception):
    '''
    Exception in case there are not tables in the db or the db doesnt exist
    '''
    pass


class ColumnError(Exception):
    '''
    Exception if the column name passed in the csv file doesnt have an appropriate
    translation: after replacing ' ' with '_', '-' with '_' and all lower case
    '''
    pass
