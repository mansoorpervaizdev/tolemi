from flask_script import Manager
from app import create_app, db
from app.models import load_csv_file


app = create_app()
manager = Manager(app)


@manager.command
def create_db():
    '''
    Drops all tables created in the db and then creates those tables again based on
    the classes in models.py
    :return:
    void
    '''
    db.drop_all()
    db.create_all()


@manager.option('-f', '--file', help="Path to data file")
def load_data(file):
    '''
    Used to load a csv data file into the db
    The path of the csv file is passed
    :param file:
    :return:
    '''
    load_csv_file(file)
    print(f'Loaded data file: {file}')


if __name__ == '__main__':
    manager.run()
