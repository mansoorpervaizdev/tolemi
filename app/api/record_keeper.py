from . import api
from app.models import Property
from flask import jsonify, make_response
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import OperationalError
from app.exceptions import NoDBSetup


@api.route('/record/<int:index>', methods=['GET'])
def get_data_record(index):
    '''
    Returns record from the dataset loaded into the db.
    Currently the dataset supported is the property tax
    pay roll available at
    https://data.providenceri.gov/Finance/2017-Property-Tax-Roll/ku9m-5rhr

    :param index: int represents 0 based index of entry in dataset
    :return:
    json dict: populated with related data
    '''
    try:
        return jsonify(get_record_at_index(index)), 200
    except NoDBSetup:
        return make_response('System set up not completed', 400)


def get_record_at_index(index_value):
    '''
    Gets the record of a matching property based on index - zero based

    :param index_value: zero based index into the property table
    :return:
    dict with values of property populated
    '''
    try:
        property_at_index = Property.query.filter_by(id=index_value + 1).one()
        return property_at_index.get_dict()
    except NoResultFound:
        return {}
    except OperationalError:
        raise NoDBSetup('DB not set up')
